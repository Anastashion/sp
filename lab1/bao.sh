#!/bin/bash

mkdir /root/files
touch /root/files/file1.txt
touch /root/files/file2.txt

echo "Разработчик: Безрукова Анастасия"
echo "Название программы: CD-диск"
echo "Описание программы: Программа для создания образа CD-диска с содержимым из указанного каталога"
echo ""

while true; do
	echo "Введите путь к каталогу с файлами: "
	read directory
	if [ -z "$directory" ]; then
		echo "Ошибка: Не указан путь к каталогу"
		continue
 	fi

	echo "Введите имя образа CD-диска: "
	read name
	if [ -z "$name" ]; then
		echo "Ошибка: Не указано имя образа CD-диска"
		continue
	fi

 	if [ -e "$name" ]; then
		name="$name-$(date +"%Y-%m-%d")"
	fi

	genisoimage -o "$name" "$directory"

	if [ -e "$name" ]; then
		echo "Образ CD-диска успешно создан: $name.iso"
        else
		echo "Ошибка: не удалось создать образ CD-диска"
        fi

	 while true; do
		echo "Cоздать еще один образ CD-диска? (y/n): "
		read answer
		case $answer in
			[Yy]* )
				directory=""
				name=""
				break
				;;
			[Nn]* )
				exit 0
				;;
                esac
         done
done
